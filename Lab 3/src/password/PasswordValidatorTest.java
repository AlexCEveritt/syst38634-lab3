package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMain() {
		assertTrue("Invalid Password Length", PasswordValidator.checkPasswordLength("theysavelives"));
	}
	
	@Test
	public void passwordLengthException() {
		assertFalse("Invalid Password Length", PasswordValidator.checkPasswordLength("lives"));
	}
	
	@Test
	public void passwordLengthBondaryIn() {
		assertTrue("Password Length Invalid", PasswordValidator.checkPasswordLength("12345678"));
	}
	
	@Test
	public void passwordLengthBoundaryOut() {
		assertFalse("Password Length Invalid", PasswordValidator.checkPasswordLength("1234567"));
	}
	
	@Test
	public void passwordDigitTest() {
		assertTrue("No Digits found in password", PasswordValidator.checkDigit("test13"));
	}
	
	@Test
	public void passwordDigitException() {
		assertFalse("Not enough digits in password", PasswordValidator.checkDigit("nodigits"));
	}
	
	@Test
	public void passwordDigitBoundaryIn() {
		assertTrue("No digits found in password", PasswordValidator.checkDigit("test12"));
		
	}
	
	@Test 
	public void passwordDigitBoundaryOut() {
		assertFalse("minimum 2 digits required", PasswordValidator.checkDigit("test1"));
	}

}
